package pl.dawidh.pathapi.infrastructure.dto;

public abstract class BaseData {
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
