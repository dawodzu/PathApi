package pl.dawidh.pathapi.infrastructure.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.dawidh.pathapi.infrastructure.import_data.ImportData;

@Component
public class ScheduledJobs {
    private static final Logger log = LoggerFactory.getLogger(ScheduledJobs.class);
    private final String startImportMassage = "Start importing files";
    private final String endImportMassage = "Finish importing files";
    private final ImportData importData;


    public ScheduledJobs(ImportData importData) {
        this.importData = importData;
    }

    @Scheduled(fixedRateString = "${import.file.scheduler.rate}")
    public void todoMethod(){
        log.info(startImportMassage);
        importData.importFile();
        log.info(endImportMassage);
    }
}
