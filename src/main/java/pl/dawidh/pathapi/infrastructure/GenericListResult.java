package pl.dawidh.pathapi.infrastructure;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class GenericListResult<T> {
    private List<T> results;

    @JsonProperty("results")
    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }

    public GenericListResult(List<T> results) {
        this.results = results;
    }
}