package pl.dawidh.pathapi.infrastructure.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
    private static final String simpleDateFormat = "yyyy-MM-dd'T'HH:mm:ssz";

    public static LocalDateTime stringToLocalDateTime(String dateToConvert){
        var formatter = DateTimeFormatter.ofPattern(simpleDateFormat, Locale.ENGLISH);
        return LocalDateTime.parse(dateToConvert, formatter);
    }

    public static Date localDateTimeToDate(LocalDateTime dateToConvert) {
        return java.util.Date
                .from(dateToConvert.atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public static LocalDateTime dateToLocalDateTime(Date dateToConvert) {
        return new java.sql.Timestamp(
                dateToConvert.getTime()).toLocalDateTime();
    }
}
