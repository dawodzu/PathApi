package pl.dawidh.pathapi.infrastructure.import_data;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.dawidh.pathapi.domain.asset.AssetDto;
import pl.dawidh.pathapi.domain.asset.AssetService;
import pl.dawidh.pathapi.domain.attribute.AttributeDto;
import pl.dawidh.pathapi.domain.attribute.AttributeService;
import pl.dawidh.pathapi.domain.base.BaseDto;
import pl.dawidh.pathapi.domain.base.BaseService;
import pl.dawidh.pathapi.domain.common.CommonDto;
import pl.dawidh.pathapi.domain.common.CommonService;
import pl.dawidh.pathapi.domain.file.FileDto;
import pl.dawidh.pathapi.domain.file.FileService;
import pl.dawidh.pathapi.domain.media_item.MediaItemDto;
import pl.dawidh.pathapi.domain.media_item.MediaItemService;
import pl.dawidh.pathapi.domain.path.PathDto;
import pl.dawidh.pathapi.domain.path.PathService;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Optional;

import static pl.dawidh.pathapi.infrastructure.utils.DateUtils.stringToLocalDateTime;

@Component
public class ImportData {

    private static final Logger log = LoggerFactory.getLogger(ImportData.class);
    private final PathService pathService;
    private final FileService fileService;
    private final MediaItemService mediaItemService;
    private final CommonService commonService;
    private final AttributeService attributeService;
    private final BaseService baseService;
    private final AssetService assetService;

    public ImportData(PathService pathService,
                      FileService fileService,
                      MediaItemService mediaItemService,
                      CommonService commonService,
                      AttributeService attributeService,
                      BaseService baseService,
                      AssetService assetService) {
        this.pathService = pathService;
        this.fileService = fileService;
        this.mediaItemService = mediaItemService;
        this.commonService = commonService;
        this.attributeService = attributeService;
        this.baseService = baseService;
        this.assetService = assetService;
    }

    @Value("${data.file.path}")
    private String dataFilePath;

    @Value("${data.file.name}")
    private String dataFileName;

    public void importFile(){
        var parser = new JSONParser();

        try (Reader reader = new FileReader(dataFilePath + dataFileName)) {
            var jsonParser =  parser.parse(reader);
            var jsonObject = (JSONObject) jsonParser;

            jsonObject.forEach((k,v) -> {
                var newPath = importPath(k.toString());
                var newFile = importFile(k.toString(), newPath);

                if (newFile.isEmpty()){
                    return;
                }

                var absolutePath = (JSONObject) jsonObject.get(k);

                var id = (String) absolutePath.get("id");
                var assets = (JSONArray) absolutePath.get("assets");

                if (!assets.isEmpty()){
                    assets.forEach(e -> processAssets((JSONObject) e, newFile.get()));
                }
            });
        } catch (IOException | ParseException e) {
            log.error(e.getMessage());
        }
    }

    private void processAssets(JSONObject asset, FileDto newFile){
        var mediaItems = (JSONArray) asset.get("media-items");
        var common = (JSONObject) asset.get("common");
        var mobId = (String) asset.get("mobId");
        var attributes = (JSONArray) asset.get("attributes");
        var base = (JSONObject) asset.get("base");

        var newAsset = new AssetDto(newFile, mobId);
        newAsset = assetService.addBase(newAsset);
        var finalNewAsset = newAsset;

        if (mediaItems != null && !mediaItems.isEmpty()) {
            mediaItems.forEach(mediaType -> processMediaItems((JSONObject) mediaType, finalNewAsset));
        }
        if (common != null && !common.isEmpty()) {
            importCommon(common, finalNewAsset);
        }
        if (attributes != null && !attributes.isEmpty()){
            attributes.forEach(attribute -> processAttributes((JSONObject) attribute, finalNewAsset));
        }
        if (base != null && !base.isEmpty()){
            importBase(base, finalNewAsset);
        }
    }

    private PathDto importPath(String absoluteFilePath){
        var path = getBasePath(absoluteFilePath);
        return (pathService.isPathAdded((path)) ?
                pathService.findPathByPathway(path) :
                pathService.savePath(new PathDto(path)));
    }

    private Optional<FileDto> importFile(String absoluteFilePath, PathDto pathDto){
        var fileName = getFileName(absoluteFilePath);
        var newFile = fileService.isFileAdded(new FileDto(fileName, pathDto));
        if (newFile.isPresent()){
            return Optional.of(fileService.convertToDto(newFile.get()));
        } else {
            return Optional.empty();
        }
    }

    private void processMediaItems(JSONObject mediaItem, AssetDto assetDto){
        var start = (String)mediaItem.get("start");
        var length = (String)mediaItem.get("length");
        var mediaMobId = (String)mediaItem.get("mobId");
        var online = (String)mediaItem.get("online");
        var essenceType = (String)mediaItem.get("essenceType");
        var track = (String)mediaItem.get("track");
        var type = (String)mediaItem.get("type");
        var newMediaType = new MediaItemDto(essenceType, track, start, length, mediaMobId, online, type);
        newMediaType.setAsset(assetDto);
        importMediaItem(newMediaType);
    }

    private void importMediaItem(MediaItemDto mediaItemDto){
        var mediaType = mediaItemService.addMediaItem(mediaItemDto);
        mediaItemService.convertToDto(mediaType);
    }

    private void importCommon(JSONObject common, AssetDto assetDto){
        var created = (String) common.get("created");
        var modified = (String) common.get("modified");
        var name = (String) common.get("name");
        var newCommon = new CommonDto(stringToLocalDateTime(created), stringToLocalDateTime(modified), name);
        newCommon.setAsset(assetDto);
        commonService.addCommon(newCommon);
    }

    private void processAttributes(JSONObject attribute, AssetDto assetDto){
        var name = (String)attribute.get("name");
        var value = (String)attribute.get("value");
        var group = (String)attribute.get("group");
        var newAttribute = new AttributeDto(group, name, value);
        newAttribute.setAsset(assetDto);
        attributeService.addAttribute(newAttribute);
    }

    private void importBase(JSONObject base, AssetDto assetDto){
        var customId = (String) base.get("id");
        var type = (String) base.get("type");
        var newBase = new BaseDto(type, customId);
        newBase.setAsset(assetDto);
        baseService.addBase(newBase);

    }

    private String getBasePath(String filePath){
        var index = filePath.lastIndexOf('/');
        return filePath.substring(0,index);
    }

    private String getFileName(String filePath){
        var index = filePath.lastIndexOf('/') + 1;
        return filePath.substring(index);
    }
}
