package pl.dawidh.pathapi.domain.media_item;

import pl.dawidh.pathapi.domain.asset.AssetEntity;
import pl.dawidh.pathapi.infrastructure.entity.BaseData;

import javax.persistence.*;

@Entity
@Table(name = "media_items")
public class MediaItemEntity extends BaseData {
    @ManyToOne
    @JoinColumn(name = "asset_id")
    private AssetEntity asset;

    private String essenceType;

    private String track;

    private String start;

    private String length;

    private String mobId;

    private String online;

    private String type;

    public AssetEntity getAsset() {
        return asset;
    }

    public void setAsset(AssetEntity asset) {
        this.asset = asset;
    }

    public String getEssenceType() {
        return essenceType;
    }

    public void setEssenceType(String essenceType) {
        this.essenceType = essenceType;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getMobId() {
        return mobId;
    }

    public void setMobId(String mobId) {
        this.mobId = mobId;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MediaItemEntity() {
    }
}
