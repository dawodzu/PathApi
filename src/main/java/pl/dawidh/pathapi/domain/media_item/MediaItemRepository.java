package pl.dawidh.pathapi.domain.media_item;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MediaItemRepository extends JpaRepository<MediaItemEntity, Long> {
    List<MediaItemEntity> findAllByAssetId(Long assetId);
}
