package pl.dawidh.pathapi.domain.media_item;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.dawidh.pathapi.domain.asset.AssetDto;
import pl.dawidh.pathapi.infrastructure.dto.BaseData;

public class MediaItemDto extends BaseData {
    private AssetDto asset;

    private String essenceType;

    private String track;

    private String start;

    private String length;

    private String mobId;

    private String online;

    private String type;

    @JsonIgnore
    public AssetDto getAsset() {
        return asset;
    }

    public void setAsset(AssetDto asset) {
        this.asset = asset;
    }

    public String getEssenceType() {
        return essenceType;
    }

    public void setEssenceType(String essenceType) {
        this.essenceType = essenceType;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getMobId() {
        return mobId;
    }

    public void setMobId(String mobId) {
        this.mobId = mobId;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MediaItemDto() {
    }

    public MediaItemDto(String essenceType, String track, String start, String length, String mobId, String online, String type) {
        this.essenceType = essenceType;
        this.track = track;
        this.start = start;
        this.length = length;
        this.mobId = mobId;
        this.online = online;
        this.type = type;
    }

    @JsonIgnore
    @Override
    public Long getId() {
        return super.getId();
    }
}
