package pl.dawidh.pathapi.domain.media_item;

import org.modelmapper.ModelMapper;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MediaItemService {

    private MediaItemRepository mediaItemRepository;
    private ModelMapper modelMapper;

    MediaItemService(MediaItemRepository mediaItemRepository, ModelMapper modelMapper){
        this.mediaItemRepository = mediaItemRepository;
        this.modelMapper = modelMapper;
    }

    public MediaItemEntity addMediaItem(MediaItemDto mediaItemDto){
        return mediaItemRepository.save(convertToEntity(mediaItemDto));
    }

    public List<MediaItemDto> getMediaItemsByAsset(Long assetId){
        return mediaItemRepository.findAllByAssetId(assetId).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    public MediaItemDto convertToDto(MediaItemEntity mediaItemEntity){
        return modelMapper.map(mediaItemEntity, MediaItemDto.class);
    }

    public MediaItemEntity convertToEntity(MediaItemDto mediaItemDto) throws ParseException {
        var mediaType = modelMapper.map(mediaItemDto, MediaItemEntity.class);
        return mediaType;
    }

}
