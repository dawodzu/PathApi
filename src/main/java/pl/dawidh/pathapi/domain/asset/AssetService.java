package pl.dawidh.pathapi.domain.asset;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import pl.dawidh.pathapi.domain.attribute.AttributeService;
import pl.dawidh.pathapi.domain.base.BaseService;
import pl.dawidh.pathapi.domain.common.CommonService;
import pl.dawidh.pathapi.domain.media_item.MediaItemService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AssetService {

    private final AssetRepository assetRepository;
    private final AttributeService attributeService;
    private final BaseService baseService;
    private final CommonService commonService;
    private final MediaItemService mediaItemService;
    private final ModelMapper modelMapper;

    AssetService(AssetRepository assetRepository, AttributeService attributeService, BaseService baseService, CommonService commonService, MediaItemService mediaItemService, ModelMapper modelMapper){
        this.assetRepository = assetRepository;
        this.attributeService = attributeService;
        this.baseService = baseService;
        this.commonService = commonService;
        this.mediaItemService = mediaItemService;
        this.modelMapper = modelMapper;
    }

    public AssetDto addBase(AssetDto assetDto){
        var savedAsset = assetRepository.save(convertToEntity(assetDto));
        return convertToDto(savedAsset);
    }

    public List<AssetDto> getAssetsByFile(Long fileId){
        var assets = assetRepository.findAllByFileId(fileId).stream()
                .map(asset -> {
                    asset.setAttributes(null);
                    asset.setBases(null);
                    asset.setCommons(null);
                    asset.setMediaItems(null);
                    return convertToDto(asset);
                })
                .collect(Collectors.toList());
        assets.forEach(asset -> {
            var attributes = attributeService.getAttributesByAsset(asset.getId());
            asset.setAttributes(attributes);

            var bases = baseService.getBasesByAsset(asset.getId());
            asset.setBase(bases);

            var commons = commonService.getCommonsByAsset(asset.getId());
            asset.setCommon(commons);

            var mediaTypes = mediaItemService.getMediaItemsByAsset(asset.getId());
            asset.setMediaItems(mediaTypes);
        });
        return assets;
    }

    public AssetDto convertToDto(AssetEntity assetEntity){
        return modelMapper.map(assetEntity, AssetDto.class);
    }

    public AssetEntity convertToEntity(AssetDto assetDto){
        return modelMapper.map(assetDto, AssetEntity.class);
    }

}
