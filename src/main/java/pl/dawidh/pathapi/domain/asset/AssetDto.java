package pl.dawidh.pathapi.domain.asset;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import pl.dawidh.pathapi.domain.attribute.AttributeDto;
import pl.dawidh.pathapi.domain.base.BaseDto;
import pl.dawidh.pathapi.domain.common.CommonDto;
import pl.dawidh.pathapi.domain.file.FileDto;
import pl.dawidh.pathapi.infrastructure.dto.BaseData;
import pl.dawidh.pathapi.domain.media_item.MediaItemDto;

import java.util.Collection;

public class AssetDto extends BaseData {

    private FileDto file;

    private Collection<AttributeDto> attributes;

    private BaseDto base;

    private Collection<MediaItemDto> mediaItems;

    private CommonDto common;

    private String mobId;

    @JsonIgnore
    public FileDto getFile() {
        return file;
    }

    public void setFile(FileDto file) {
        this.file = file;
    }

    public Collection<AttributeDto> getAttributes() {
        return attributes;
    }

    public void setAttributes(Collection<AttributeDto> attributes) {
        this.attributes = attributes;
    }

    public BaseDto getBase() {
        return base;
    }

    public void setBase(BaseDto base) {
        this.base = base;
    }

    public Collection<MediaItemDto> getMediaItems() {
        return mediaItems;
    }

    @JsonProperty("media-items")
    public void setMediaItems(Collection<MediaItemDto> mediaItems) {
        this.mediaItems = mediaItems;
    }

    public CommonDto getCommon() {
        return common;
    }

    public void setCommon(CommonDto common) {
        this.common = common;
    }

    public String getMobId() {
        return mobId;
    }

    public void setMobId(String mobId) {
        this.mobId = mobId;
    }

    public AssetDto(FileDto file, String mobId) {
        this.file = file;
        this.mobId = mobId;
    }

    public AssetDto() {
    }

    @JsonIgnore
    @Override
    public Long getId() {
        return super.getId();
    }
}
