package pl.dawidh.pathapi.domain.asset;

import pl.dawidh.pathapi.domain.attribute.AttributeEntity;
import pl.dawidh.pathapi.domain.base.BaseEntity;
import pl.dawidh.pathapi.domain.common.CommonEntity;
import pl.dawidh.pathapi.domain.file.FileEntity;
import pl.dawidh.pathapi.domain.media_item.MediaItemEntity;
import pl.dawidh.pathapi.infrastructure.entity.*;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "assets")
public class AssetEntity extends BaseData {
    @ManyToOne
    @JoinColumn(name="file_id")
    private FileEntity file;

    @OneToMany(mappedBy = "asset", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<AttributeEntity> attributes;

    @OneToOne(mappedBy = "asset", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private BaseEntity bases;

    @OneToMany(mappedBy = "asset", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<MediaItemEntity> mediaItems;

    @OneToOne(mappedBy = "asset", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private CommonEntity commons;

    private String mobId;

    public FileEntity getFile() {
        return file;
    }

    public void setFile(FileEntity file) {
        this.file = file;
    }

    public void setAttributes(Collection<AttributeEntity> attributes) {
        this.attributes = attributes;
    }

    public void setBases(BaseEntity bases) {
        this.bases = bases;
    }

    public void setMediaItems(Collection<MediaItemEntity> mediaItems) {
        this.mediaItems = mediaItems;
    }

    public void setCommons(CommonEntity commons) {
        this.commons = commons;
    }

    public String getMobId() {
        return mobId;
    }

    public void setMobId(String mobId) {
        this.mobId = mobId;
    }

    public AssetEntity(Long id) {
        this.id = id;
    }

    public AssetEntity() {
    }
}
