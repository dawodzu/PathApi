package pl.dawidh.pathapi.domain.file;

import org.springframework.web.bind.annotation.*;
import pl.dawidh.pathapi.infrastructure.exception.BadRequestException;

import java.util.List;

@RestController
@RequestMapping(path = "/",
        consumes = "application/json",
        produces = "application/json")
public class FileController {

    private final FileService fileService;
    private final String badParamMassage = "Parameters can not be negative";
    private final String badTypesMassage = "allowed types: %s";

    private FileController(FileService pathService){
        this.fileService = pathService;
    }

    @GetMapping(path = {"/{folderId}"})
    private @ResponseBody
    List<FileDto> getFolderContent(@PathVariable String folderId,
                                   @RequestParam(required = false, defaultValue = "") String type,
                                   @RequestParam(required = false) Integer skip,
                                   @RequestParam(required = false) Integer limit){
        if((skip != null && skip < 0) || (limit != null && limit < 0))
            throw new BadRequestException(badParamMassage);
        if (!fileService.checkType(type))
            throw new BadRequestException(String.format(badTypesMassage, fileService.getAllowedTypes()));
        return fileService.getFilesByPath(folderId, limit, skip, type);
    }
}
