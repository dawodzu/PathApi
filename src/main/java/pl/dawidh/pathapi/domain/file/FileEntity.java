package pl.dawidh.pathapi.domain.file;

import pl.dawidh.pathapi.domain.asset.AssetEntity;
import pl.dawidh.pathapi.infrastructure.entity.BaseData;
import pl.dawidh.pathapi.domain.path.PathEntity;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "files")
public class FileEntity extends BaseData {
    private String name;

    @OneToMany(mappedBy="file", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<AssetEntity> assetsCollections;

    @ManyToOne
    @JoinColumn(name="path_id")
    private PathEntity path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAssetsCollections(Collection<AssetEntity> assetsCollections) {
        this.assetsCollections = assetsCollections;
    }

    public PathEntity getPath() {
        return path;
    }

    public void setPath(PathEntity path) {
        this.path = path;
    }

    public FileEntity() {
    }
}
