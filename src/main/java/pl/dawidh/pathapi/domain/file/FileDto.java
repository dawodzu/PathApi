package pl.dawidh.pathapi.domain.file;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.dawidh.pathapi.domain.asset.AssetDto;
import pl.dawidh.pathapi.infrastructure.dto.BaseData;
import pl.dawidh.pathapi.domain.path.PathDto;

import java.util.Collection;

public class FileDto extends BaseData {
    private String name;

    private Collection<AssetDto> assets;

    private PathDto path;

    @JsonIgnore
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<AssetDto> getAssets() {
        return assets;
    }

    public void setAssets(Collection<AssetDto> assets) {
        this.assets = assets;
    }

    @JsonIgnore
    public PathDto getPath() {
        return path;
    }

    public void setPath(PathDto path) {
        this.path = path;
    }

    public FileDto(String name, PathDto path) {
        this.name = name;
        this.path = path;
    }

    public FileDto() {
    }
}
