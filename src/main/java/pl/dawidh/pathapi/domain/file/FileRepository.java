package pl.dawidh.pathapi.domain.file;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface FileRepository extends JpaRepository<FileEntity, Long> {
    Optional<FileEntity> findByNameEqualsIgnoreCaseAndPathId(String file, Long pathId);

    @Query(value = ("select f.* from files f where f.path_id = :pathId limit :skip,:limit"), nativeQuery = true)
    List<FileEntity> findAllByPathId(@Param("pathId") Long pathId,
                                      @Param("skip") Integer skip,
                                      @Param("limit") Integer limit);

    @Query(value = ("select f.* from files f " +
            "left join assets a on f.id=a.file_id " +
            "left join bases b on a.id=b.asset_id " +
            "where f.path_id = :pathId and b.type = :type " +
            "limit :skip,:limit"), nativeQuery = true)
    List<FileEntity> findAllByBaseType(@Param("pathId") Long pathId,
                                        @Param("type") String type,
                                        @Param("skip") Integer skip,
                                        @Param("limit") Integer limit);
}
