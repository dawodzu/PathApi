package pl.dawidh.pathapi.domain.file;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.dawidh.pathapi.domain.asset.AssetService;
import pl.dawidh.pathapi.domain.path.PathService;
import pl.dawidh.pathapi.infrastructure.exception.BadRequestException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FileService {
    private static final Logger log = LoggerFactory.getLogger(FileService.class);
    private final FileRepository fileRepository;
    private final PathService pathService;
    private final AssetService assetService;
    private final ModelMapper modelMapper;

    private final String parseNumberErrorMassage = "%s could not be converted to a number";

    @Value("${file.allowed.types}")
    private List<String> allowedTypes;

    public FileService(FileRepository fileRepository, PathService pathService, AssetService assetService, ModelMapper modelMapper) {
        this.fileRepository = fileRepository;
        this.pathService = pathService;
        this.assetService = assetService;
        this.modelMapper = modelMapper;
    }

    List<FileDto> getFilesByPath(String pathId, Integer limit, Integer skip, String type){
        var path = pathService.findPathById(stringToLong(pathId));

        var files = (type.equals("")) ?
                convertFilesToDto(fileRepository.findAllByPathId(path.getId(),
                        (skip == null) ? 0 : skip,
                        (limit == null ) ? -1 : limit)) :
                convertFilesToDto(fileRepository.findAllByBaseType(path.getId(), type,
                        (skip == null) ? 0 : skip,
                        (limit == null ) ? -1 : limit));

        files.forEach(file -> {
            var assets = assetService.getAssetsByFile(file.getId());
            file.setAssets(assets);
        });
        return files;
    }

    boolean checkType(String type){
        if(type.equals("")){
            return true;
        } else {
            return allowedTypes.contains(type);
        }
    }

    List<String> getAllowedTypes(){
        return allowedTypes;
    }

    public Optional<FileEntity> isFileAdded(FileDto fileDto){
        if (fileRepository.findByNameEqualsIgnoreCaseAndPathId(fileDto.getName(), fileDto.getPath().getId()).isEmpty()){
            return Optional.of(fileRepository.save(convertToEntity(fileDto)));
        } else {
            return Optional.empty();
        }
    }

    public FileDto convertToDto(FileEntity fileEntity){
        return modelMapper.map(fileEntity, FileDto.class);
    }

    private FileEntity convertToEntity(FileDto fileDto){
        return modelMapper.map(fileDto, FileEntity.class);
    }

    private List<FileDto> convertFilesToDto(List<FileEntity> files){
        return files.stream()
                .map(file -> {
                    file.setAssetsCollections(null);
                    return convertToDto(file);
                })
                .collect(Collectors
                        .toList());
    }

    private Long stringToLong(String string){
        try{
            return Long.valueOf(string);
        } catch(NumberFormatException ex){
            log.error(ex.getMessage());
            throw new BadRequestException(String.format(parseNumberErrorMassage, string));
        }
    }

}
