package pl.dawidh.pathapi.domain.attribute;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.dawidh.pathapi.domain.asset.AssetDto;
import pl.dawidh.pathapi.infrastructure.dto.BaseData;

public class AttributeDto extends BaseData {
    private AssetDto asset;

    private String group;

    private String name;

    private String value;

    @JsonIgnore
    public AssetDto getAsset() {
        return asset;
    }

    public void setAsset(AssetDto asset) {
        this.asset = asset;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public AttributeDto(String group, String name, String value) {
        this.group = group;
        this.name = name;
        this.value = value;
    }

    public AttributeDto() {
    }

    @JsonIgnore
    @Override
    public Long getId() {
        return super.getId();
    }
}
