package pl.dawidh.pathapi.domain.attribute;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AttributeRepository extends JpaRepository<AttributeEntity, Long> {
    List<AttributeEntity> findAllByAssetId (Long assetId);
}
