package pl.dawidh.pathapi.domain.attribute;

import pl.dawidh.pathapi.domain.asset.AssetEntity;
import pl.dawidh.pathapi.infrastructure.entity.BaseData;

import javax.persistence.*;

@Entity
@Table(name = "attributes")
public class AttributeEntity extends BaseData {
    @ManyToOne
    @JoinColumn(name = "asset_id")
    private AssetEntity asset;

    @Column(name = "group__")
    private String group;

    private String name;

    private String value;

    public AssetEntity getAsset() {
        return asset;
    }

    public void setAsset(AssetEntity asset) {
        this.asset = asset;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public AttributeEntity() {
    }
}
