package pl.dawidh.pathapi.domain.attribute;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AttributeService {

    private AttributeRepository attributeRepository;
    private ModelMapper modelMapper;

    AttributeService(AttributeRepository attributeRepository, ModelMapper modelMapper){
        this.attributeRepository = attributeRepository;
        this.modelMapper = modelMapper;
    }

    public AttributeEntity addAttribute(AttributeDto attributeDto){
        return attributeRepository.save(convertToEntity(attributeDto));
    }

    public List<AttributeDto> getAttributesByAsset(Long assetId){
        return attributeRepository.findAllByAssetId(assetId).stream()
            .map(this::convertToDto)
            .collect(Collectors.toList());
    }

    public AttributeDto convertToDto(AttributeEntity attributeEntity){
        return modelMapper.map(attributeEntity, AttributeDto.class);
    }

    public AttributeEntity convertToEntity(AttributeDto attributeDto){
        return modelMapper.map(attributeDto, AttributeEntity.class);
    }

}
