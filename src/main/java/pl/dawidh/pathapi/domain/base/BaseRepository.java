package pl.dawidh.pathapi.domain.base;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BaseRepository extends JpaRepository<BaseEntity, Long> {
    Optional<BaseEntity> findByAssetId(Long assetId);
}
