package pl.dawidh.pathapi.domain.base;

import pl.dawidh.pathapi.domain.asset.AssetEntity;
import pl.dawidh.pathapi.infrastructure.entity.BaseData;

import javax.persistence.*;

@Entity
@Table(name = "bases")
public class BaseEntity extends BaseData {
    @OneToOne
    @JoinColumn(name = "asset_id")
    private AssetEntity asset;

    private String type;

    @Column(name = "custom_id")
    private String customId;

    public AssetEntity getAsset() {
        return asset;
    }

    public void setAsset(AssetEntity asset) {
        this.asset = asset;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }
}
