package pl.dawidh.pathapi.domain.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import pl.dawidh.pathapi.domain.asset.AssetDto;
import pl.dawidh.pathapi.infrastructure.dto.BaseData;

public class BaseDto extends BaseData {
    private AssetDto asset;

    private String type;

    private String customId;

    @JsonIgnore
    public AssetDto getAsset() {
        return asset;
    }

    public void setAsset(AssetDto asset) {
        this.asset = asset;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("id")
    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public BaseDto(String type, String customId) {
        this.type = type;
        this.customId = customId;
    }

    public BaseDto() {
    }

    @JsonIgnore
    @Override
    public Long getId() {
        return super.getId();
    }
}
