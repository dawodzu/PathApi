package pl.dawidh.pathapi.domain.base;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class BaseService {

    private BaseRepository baseRepository;
    private ModelMapper modelMapper;

    BaseService(BaseRepository baseRepository, ModelMapper modelMapper){
        this.baseRepository = baseRepository;
        this.modelMapper = modelMapper;
    }

    public BaseEntity addBase(BaseDto baseDto){
        return baseRepository.save(convertToEntity(baseDto));
    }

    public BaseDto getBasesByAsset(Long assetId){
        return baseRepository.findByAssetId(assetId).map(this::convertToDto).orElseGet(BaseDto::new);
    }

    public BaseDto convertToDto(BaseEntity baseEntity){
        return modelMapper.map(baseEntity, BaseDto.class);
    }

    public BaseEntity convertToEntity(BaseDto baseDto){
        return modelMapper.map(baseDto, BaseEntity.class);
    }

}
