package pl.dawidh.pathapi.domain.path;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PathRepository extends JpaRepository<PathEntity, Long> {
    Optional<PathEntity> findByPathEqualsIgnoreCase(String path);

    @Query(value = ("select p.* from paths p limit :skip,:limit"), nativeQuery = true)
    List<PathEntity> findAll(@Param("skip") Integer skip,
                             @Param("limit") Integer limit);

    @Query(value = ("select p.* from paths p where p.path like %:path% limit :skip,:limit"), nativeQuery = true)
    List<PathEntity> findAllByPathContains(@Param("path") String path,
                                           @Param("skip") Integer skip,
                                           @Param("limit") Integer limit);
}
