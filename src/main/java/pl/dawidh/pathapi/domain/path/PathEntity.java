package pl.dawidh.pathapi.domain.path;

import pl.dawidh.pathapi.domain.file.FileEntity;
import pl.dawidh.pathapi.infrastructure.entity.BaseData;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "paths")
public class PathEntity extends BaseData {
    private String path;

    @OneToMany(mappedBy="path", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<FileEntity> files;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setFiles(Collection<FileEntity> files) {
        this.files = files;
    }

    public PathEntity() {
    }
}
