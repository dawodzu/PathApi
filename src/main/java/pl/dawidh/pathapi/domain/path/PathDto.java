package pl.dawidh.pathapi.domain.path;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.dawidh.pathapi.domain.file.FileDto;
import pl.dawidh.pathapi.infrastructure.dto.BaseData;

import java.util.Collection;

public class PathDto extends BaseData {
    private String path;
    private Collection<FileDto> files;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @JsonIgnore
    public Collection<FileDto> getFiles() {
        return files;
    }

    public void setFiles(Collection<FileDto> files) {
        this.files = files;
    }

    public PathDto(String path) {
        this.path = path;
    }

    public PathDto() {
    }

}
