package pl.dawidh.pathapi.domain.path;

import org.springframework.web.bind.annotation.*;
import pl.dawidh.pathapi.infrastructure.GenericListResult;
import pl.dawidh.pathapi.infrastructure.exception.BadRequestException;

@RestController
@RequestMapping(path = "/",
        consumes = "application/json",
        produces = "application/json")
public class PathController {

    private final PathService pathService;
    private final String badParamMassage = "Parameters can not be negative";

    private PathController(PathService pathService){
        this.pathService = pathService;
    }

    @GetMapping(path = {""})
    private @ResponseBody
    GenericListResult getFolderList(@RequestParam(required = false, defaultValue = "") String query,
                                    @RequestParam(required = false) Integer skip,
                                    @RequestParam(required = false) Integer limit){
        if((skip != null && skip < 0) || (limit != null && limit < 0))
            throw new BadRequestException(badParamMassage);
        return pathService.getFolderList(query, limit, skip);
    }

}
