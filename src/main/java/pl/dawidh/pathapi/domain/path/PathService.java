package pl.dawidh.pathapi.domain.path;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import pl.dawidh.pathapi.infrastructure.GenericListResult;
import pl.dawidh.pathapi.infrastructure.exception.NotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PathService {
    private final PathRepository pathRepository;
    private final ModelMapper modelMapper;

    private final String notFoundByIdMassage = "folder with provided ID not found";
    private final String notFoundByPathMassage = "folder with provided path not found";

    PathService(PathRepository pathRepository, ModelMapper modelMapper){
        this.pathRepository = pathRepository;
        this.modelMapper = modelMapper;
    }

    public PathDto savePath(PathDto pathDto){
        return convertToDto(pathRepository.save(convertToEntity(pathDto)));
    }

    public boolean isPathAdded(String path){
        return pathRepository.findByPathEqualsIgnoreCase(path).isPresent();
    }

    public PathDto findPathById(Long pathId){
        var path = pathRepository.findById(pathId).orElseThrow(() -> {
            throw new NotFoundException(notFoundByIdMassage);
        });
        return convertToDto(path);
    }

    public PathDto findPathByPathway(String pathway){
        var path =  pathRepository.findByPathEqualsIgnoreCase(pathway).orElseThrow(() -> {
            throw new NotFoundException(notFoundByPathMassage);
        });
        return convertToDto(path);
    }

    GenericListResult getFolderList(String query, Integer limit, Integer skip){
        var paths = (query.equals("")) ?
                convertPathsToDto(pathRepository.findAll((skip == null) ? 0 : skip,
                        (limit == null ) ? -1 : limit)) :
                convertPathsToDto(pathRepository.findAllByPathContains(query,
                        (skip == null) ? 0 : skip,
                        (limit == null ) ? -1 : limit));
        return new GenericListResult(paths);
    }

    public PathDto convertToDto(PathEntity pathEntity){
        return modelMapper.map(pathEntity, PathDto.class);
    }

    private PathEntity convertToEntity(PathDto pathDto){
        return modelMapper.map(pathDto, PathEntity.class);
    }

    private List<PathDto> convertPathsToDto(List<PathEntity> paths){
        return paths.stream()
                .map(this::convertToDto)
                .collect(Collectors
                        .toCollection(ArrayList::new));
    }

}
