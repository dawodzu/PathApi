package pl.dawidh.pathapi.domain.common;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CommonRepository extends JpaRepository<CommonEntity, Long> {
    Optional<CommonEntity> findByAssetId(Long assetId);
}
