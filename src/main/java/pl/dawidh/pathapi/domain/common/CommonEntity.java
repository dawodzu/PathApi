package pl.dawidh.pathapi.domain.common;

import pl.dawidh.pathapi.domain.asset.AssetEntity;
import pl.dawidh.pathapi.infrastructure.entity.BaseData;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

import static pl.dawidh.pathapi.infrastructure.utils.DateUtils.dateToLocalDateTime;
import static pl.dawidh.pathapi.infrastructure.utils.DateUtils.localDateTimeToDate;

@Entity
@Table(name = "commons")
public class CommonEntity extends BaseData {
    @OneToOne
    @JoinColumn(name = "asset_id")
    private AssetEntity asset;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    private String name;

    public AssetEntity getAsset() {
        return asset;
    }

    public void setAsset(AssetEntity asset) {
        this.asset = asset;
    }

    public LocalDateTime getCreated() {
        return dateToLocalDateTime(created);
    }

    public void setCreated(LocalDateTime created) {
        this.created = localDateTimeToDate(created);
    }

    public LocalDateTime getModified() {
        return dateToLocalDateTime(modified);
    }

    public void setModified(LocalDateTime modified) {
        this.modified = localDateTimeToDate(modified);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CommonEntity() {
    }
}
