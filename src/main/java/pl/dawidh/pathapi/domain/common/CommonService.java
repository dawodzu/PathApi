package pl.dawidh.pathapi.domain.common;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;


@Service
public class CommonService {

    private CommonRepository commonRepository;
    private ModelMapper modelMapper;

    CommonService(CommonRepository commonRepository, ModelMapper modelMapper){
        this.commonRepository = commonRepository;
        this.modelMapper = modelMapper;
    }

    public CommonDto getCommonsByAsset(Long assetId){
        return commonRepository.findByAssetId(assetId).map(this::convertToDto).orElseGet(CommonDto::new);
    }

    public CommonEntity addCommon(CommonDto commonDto){
        return commonRepository.save(convertToEntity(commonDto));
    }

    public CommonDto convertToDto(CommonEntity commonEntity){
        return modelMapper.map(commonEntity, CommonDto.class);
    }

    private CommonEntity convertToEntity(CommonDto commonDto){
        return modelMapper.map(commonDto, CommonEntity.class);
    }

}
