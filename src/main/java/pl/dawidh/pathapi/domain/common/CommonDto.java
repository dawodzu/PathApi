package pl.dawidh.pathapi.domain.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.dawidh.pathapi.domain.asset.AssetDto;
import pl.dawidh.pathapi.infrastructure.dto.BaseData;

import java.time.LocalDateTime;

public class CommonDto extends BaseData {
    private AssetDto asset;

    private LocalDateTime created;

    private LocalDateTime  modified;

    private String name;

    @JsonIgnore
    public AssetDto getAsset() {
        return asset;
    }

    public void setAsset(AssetDto asset) {
        this.asset = asset;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CommonDto(LocalDateTime created, LocalDateTime modified, String name) {
        this.created = created;
        this.modified = modified;
        this.name = name;
    }

    public CommonDto() {
    }

    @JsonIgnore
    @Override
    public Long getId() {
        return super.getId();
    }
}
