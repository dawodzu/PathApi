## requirements
* Java 12

## file import
* application.properties set path and file name before app starting

## endpoint [GET]
* folder paths: /  
query - serach path contains text  
skip - skip x records  
limit - limit size to x  
/?query=test&skip=1&limit=1  

* folder content: /{folderId}  
folderId - folder id  
type - search files contains type  
skip - skip x records  
limit - limit size to x  
/1?type=group&skip=1&limit=1
