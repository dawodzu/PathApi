FROM maven:3.6.1-jdk-12
RUN mkdir /opt/app

WORKDIR /opt/app

COPY . /opt/app
RUN mvn package

CMD ["java", "-jar", "/opt/app/target/path-api-1.0.1.jar"]